// ignore_for_file: deprecated_member_use, prefer_const_constructors

import 'package:app_develop/login.dart';
import 'package:flutter/material.dart';

class MyRegister extends StatefulWidget {
  const MyRegister({Key? key}) : super(key: key);

  @override
  State<MyRegister> createState() => _MyRegisterState();
}

class _MyRegisterState extends State<MyRegister> {
  @override
  Widget build(BuildContext context) {
         return Scaffold(
       appBar: new AppBar(
      title: Text("Create your account"),
      backgroundColor: Colors.blue,
    ), 
     backgroundColor: Colors.blue[100],
    body: Container(
      padding: EdgeInsets.all(10.0),
      child: GridView.count(
        crossAxisCount: 2,
      children:  [
         Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.2, right: 35, left: 35),
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Display name',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextField(
                    obscureText:  true,
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Email',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                 SizedBox(
                    height: 30,
                  ),
                  TextField(
                    obscureText:  true,
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Password',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      TextButton(
                        onPressed: () { 
                          Navigator.push(context, MaterialPageRoute(builder: (context) => MyLogin()),);
                        }, 
                        child: Text(
                          'Sign in', 
                          style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: 12,
                        color: Colors.blueGrey,
                        ),
                        ) )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  FlatButton(
                    color: Colors.blueGrey,
                    onPressed: () {}, 
                    child: Text('Register'),
                    ),
                ],
              ),
            ),
      ]
      ),
    ),
        );
     
  }
}