// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: prefer_const_constructors



import 'package:app_develop/form.dart';
import 'package:app_develop/login.dart';
import 'package:app_develop/profile.dart';

import 'package:app_develop/register.dart';
import 'package:app_develop/settings.dart';


import 'package:flutter/material.dart';
import 'package:app_develop/dashboard.dart';
import 'package:app_develop/contacts.dart';




void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: 'login',
    routes: {
      'login' : (context) =>MyLogin(),
      'register':(context) => MyRegister(),
      'dashboard':(context) => MyDashboard(),
      'Contact':(context) => MyContacts(),
      'profile':(context) => MyProfile(),
      'settings':(context) => MySettings(),
      'form':(context) => MyForm(),

      
 
      },
      home: MyLogin(),
  ));
}


