// ignore_for_file: prefer_const_constructors, deprecated_member_use

import 'package:flutter/material.dart';


class MyForm extends StatefulWidget {
  const MyForm({Key? key}) : super(key: key);

  @override
  State<MyForm> createState() => _MyFormState();
}

class _MyFormState extends State<MyForm> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       // ignore: unnecessary_new
       appBar: new AppBar(
      title: Text("Fill the Form"),
      backgroundColor: Colors.blue,
    ), 
     backgroundColor: Colors.blue[100],
    body: Container(
      padding: EdgeInsets.all(10.0),
      child: GridView.count(
        crossAxisCount: 2,
      children:  [
         Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.2, right: 35, left: 35),
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Name',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                 
                 SizedBox(
                    height: 30,
                  ),
                  TextField(
                    obscureText:  true,
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Surname',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextField(
                    obscureText:  true,
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'ID Number',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                  
                  SizedBox(
                    height: 20,
                  ),
                  FlatButton(
                    color: Colors.blueGrey,
                    onPressed: () {}, 
                    child: Text('Save'),
                    ),
                ],
              ),
            ),
      ]
      ),
    ),
        );
     
  }
}

    
  
