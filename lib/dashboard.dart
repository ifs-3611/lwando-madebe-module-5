// ignore_for_file: prefer_const_constructors, unnecessary_new

import 'package:app_develop/contacts.dart';
import 'package:app_develop/form.dart';
// ignore: unused_import
import 'package:app_develop/gallery.dart';
import 'package:app_develop/profile.dart';
import 'package:app_develop/settings.dart';

import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe, unused_import



class MyDashboard extends StatefulWidget {
  const MyDashboard({Key? key}) : super(key: key);

  @override
  State<MyDashboard> createState() => _MyDashboardState();
}

class _MyDashboardState extends State<MyDashboard> {
  @override
  Widget build(BuildContext context) {
    // ignore: prefer_typing_uninitialized_variables
  
    return Scaffold(
    appBar: new AppBar(
      title: Text("Dashboard"),
      backgroundColor: Colors.blue,
    ), 
    backgroundColor: Colors.blue[100],
    body: Container(
      padding: EdgeInsets.all(30.0),
      child: GridView.count(
        crossAxisCount: 2,
        children: <Widget> [
          
          Card(
            color: Colors.green,
            margin: EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
               Navigator.push(context, MaterialPageRoute(builder: (context) => MyContacts()),);
              },
              splashColor: Colors.blue,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children:<Widget> [
                    Icon(Icons.contacts, size: 70.0,),
                    Text("Contacts us", style: new TextStyle(fontSize: 17.0)),
                  ],
                ),
              ),
            ),
          ),
          Card(
            color: Colors.green,
            margin: EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {   
                Navigator.push(context, MaterialPageRoute(builder: (context) => MySettings()),); 
              },
              splashColor: Colors.blue,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children:<Widget> [
                    Icon(Icons.settings, size: 70.0,),
                    Text("Settings", style: new TextStyle(fontSize: 17.0))
                  ],
                ),
              ),
            ),
          ),
          Card(
            color: Colors.green,
            margin: EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
              },
              splashColor: Colors.blue,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  // ignore: prefer_const_literals_to_create_immutables
                  children:<Widget> [
                    Icon(Icons.add_a_photo_outlined, size: 70.0,),
                    Text("Gallary", style: new TextStyle(fontSize: 17.0))
                  ],
                ),
              ),
            ),
          ),
          Card(
            color: Colors.green,
            margin: EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => MyProfile()),);
              },
              splashColor: Colors.blue,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  // ignore: prefer_const_literals_to_create_immutables
                  children:<Widget> [
                    Icon(Icons.account_box, size: 70.0,),
                    Text("User Edit profile", style: new TextStyle(fontSize: 17.0))
                  ],
                ),
              ),
            ),
          ),
          Card(
            color: Colors.green,
            margin: EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => MyForm()),);
              },
              splashColor: Colors.blue,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  // ignore: prefer_const_literals_to_create_immutables
                  children:<Widget> [
                    Icon(Icons.assignment, size: 70.0,),
                    Text("Form", style: new TextStyle(fontSize: 17.0))
                  ],
                ),
              ),
            ),
          ),
          
        ],
        ),
    ),
    
   );
  
  }
}

