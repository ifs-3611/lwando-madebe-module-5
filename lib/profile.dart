// ignore_for_file: prefer_const_constructors

import 'package:app_develop/login.dart';
import 'package:flutter/material.dart';


class MyProfile extends StatefulWidget {
  const MyProfile({Key? key}) : super(key: key);

  @override
  State<MyProfile> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // ignore: unnecessary_new
      appBar: new AppBar(
      title: Text("Profile"),
      backgroundColor: Colors.blue,
    ), 
     backgroundColor: Colors.blue[100],
    body: Container(
      padding: EdgeInsets.all(30.0),
      child: GridView.count(
        crossAxisCount: 2,
      children:  [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            decoration: InputDecoration(
            fillColor: Colors.grey.shade200,
            filled: true,
            hintText: 'Name',
            border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10)
          ),
       ),
      ),
    ),
    
    Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            decoration: InputDecoration(
            fillColor: Colors.grey.shade200,
            filled: true,
            hintText: 'Surname',
            border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10)
          ),
       ),
      ),
    ),
    
    Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            decoration: InputDecoration(
            fillColor: Colors.grey.shade200,
            filled: true,
            hintText: 'Email',
            border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10)
          ),
       ),
      ),
    ),
    
    Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            decoration: InputDecoration(
            fillColor: Colors.grey.shade200,
            filled: true,
            hintText: 'Phone Number',
            border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10)
          ),
       ),
      ),
    ),
    
    ]
   ),
      ),
    );
    
 }
}